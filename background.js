 var value = 'Sample storage chrome.';
 var current_btc_price = '';
 var current_platform = '';
 var badgeTexte;

 chrome.runtime.onInstalled.addListener(() => {
    console.log('onInstalled...');
    //chrome.browserAction.setBadgeText({text: '2000.00'});
    // chrome.browserAction.setBadgeBackgroundColor({color: "red"});
      badgeTexte = new BadgeTextAnimator( {
      text: ' ********* COINBASE : 2039 --- BITSTAMP : 1009',
      interval: 300,
      repeat: true,
      size: 200 
    } );
    chrome.browserAction.setBadgeBackgroundColor({color: "black"});
    badgeTexte.animate();

    chrome.storage.sync.set({current_crypto: 'btc', current_devise: 'usd' }, function() {
      console.log('Values OK');
    });
    
    chrome.alarms.create('refresh', { periodInMinutes: 0.03 });
    });
  
 chrome.alarms.onAlarm.addListener((alarm) => {
   // POUR METTRE A JOUR LE BADGE badgeTexte.options.text = 'BTC : '+ 'valeur' +' Ethereum 200'// etc
   console.log(badgeTexte.options.text)
  //console.dir(alarm);
    httpGetAsync('https://coinbase.com/api/v2/assets/prices?base=EUR&filter=listed&resolution=latest&', callback);
    // Send a message from background script to current content tab
    
    chrome.tabs.query({}, function(tabs) {
      for(var i = 0; i < tabs.length; i++){
        pageURL = tabs[i].url;
        isChrome = pageURL.indexOf('chrome:') >= 0;
        if(!isChrome){
          chrome.tabs.sendMessage(tabs[i].id, {
            code: tabs[i].url, 
            tab: "Popup", 
            price_btc: current_btc_price, 
            price_eth: current_eth_price,
            price_ltc: current_ltc_price,
            price_bch: current_bch_price
          }, 
          function(response) {
          console.log("Sent message, response is : " + response.farewell);
          });
        }
      }
    });
  }); 

  chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
});



 
  function httpGetAsync(theUrl, callback)
{
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() { 
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200 || xmlHttp.status == 404)
            callback(xmlHttp.responseText);
    }
    xmlHttp.open("GET", theUrl, true); // true for asynchronous 
    xmlHttp.send(null);
}

function callback(string){
  price = string
  price_parsed = JSON.parse(price)
  current_btc_price = (price_parsed.data[0].prices.latest);
  current_eth_price = (price_parsed.data[3].prices.latest);
  current_ltc_price = (price_parsed.data[5].prices.latest);
  current_bch_price = (price_parsed.data[1].prices.latest);
  console.log('BTC : ' +  current_btc_price)
  console.log('ETH : ' +  current_eth_price)
  console.log('LTC : ' +  current_ltc_price)
  console.log('BCH : ' +  current_bch_price)
  //chrome.alarms.create('sendsource', { delayInMinutes: 0 });
}

/* 
  MAYBE IN A FUTURE UPDATE
*/

/*
function getContentFromClipboard() {
  var result = '';
  var sandbox = document.getElementById('sandbox');
  sandbox.value = '';
  sandbox.select();
  if (document.execCommand('paste')) {
      result = sandbox.value;
      //console.log('got value from sandbox: ' + result);
  }
  sandbox.value = '';
  return result;
}

*/





function BadgeTextAnimator ( options ) {
	if ( options == null ) {
		throw new Error( 'You must pass options to the BadgeTextAnimator' );
	}

	this.options = {
		text: options.text,
		interval: ( options.interval == null ? 500 : options.interval ),
		repeat: ( options.repeat == null ? true : options.repeat ),
		size: ( options.size != null && options.size > 0 && options.size <= 6 ? options.size : 6 )
	};

	this._intervalId = null;
	this._currentIndex = 0;
}


BadgeTextAnimator.prototype.animate = function () {
	var spaces = [ '', ' ', '  ', '   ', '    ', '     ', '      ' ];
	this._setBadgeText( spaces[this.options.size] );

	this._doAnimate();

	this._intervalId = setInterval(
		function () {
			this._doAnimate();
		}.bind( this ),
		this.options.interval
	);
};


BadgeTextAnimator.prototype.stop = function () {
	clearInterval( this._intervalId );
	this._intervalId = null;

	this._setBadgeText( '' );
};

BadgeTextAnimator.prototype._doAnimate = function () {
	var startAt = this._currentIndex,
		cutAt = this.options.size,
		addBefore = false,
		chunk, difference;

	if ( this._currentIndex < this.options.size ) {
		cutAt = this._currentIndex + 1;
		addBefore = true;
		startAt = 0;
	}

	chunk = this.options.text.substr( startAt, cutAt );

	if ( chunk.length < this.options.size ) {
		difference = this.options.size - chunk.length;
		for ( var i = 0; i <= difference; i++ ) {
			if ( addBefore === true ) {
				chunk = ' ' + chunk;
			} else {
				chunk = chunk + ' ';
			}
		}
	}

	this._setBadgeText( chunk );

	this._currentIndex = this._currentIndex + 1;
	if ( this._currentIndex === this.options.text.length ) {
		if ( this.options.repeat === true ) {
			this._currentIndex = 0;
		} else {
			this.stop();
		}
	}
};

BadgeTextAnimator.prototype._setBadgeText = function ( text ) {
	chrome.browserAction.setBadgeText( { text: text } );
};